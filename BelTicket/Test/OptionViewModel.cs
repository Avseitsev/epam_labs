﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class OptionViewModel : Screen, INotifyPropertyChanged
    {
        private readonly IWindowManager _windowManager;

        public OptionViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }

      //  public OptionViewModel()
      //  {
      //  }

        public void ShowDialog()
        {
            var vm = new TaskViewModel(zaprosi, duration, potoci, url);
            _windowManager.ShowDialog(vm);
        }

    private int zaprosi;
        private int duration;
        private int potoci;
        private string url;

        public int Zaprosi
        {
            get { return zaprosi; }
            set
            {
                zaprosi = value;
                OnPropertyChanged("Zaprosi");
            }
        }
        public int Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                OnPropertyChanged("Duration");
            }
        }
        public int Potoci
        {
            get { return potoci; }
            set
            {
                potoci = value;
                OnPropertyChanged("Potoci");
            }
        }
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                OnPropertyChanged("Title");
            }
        }


       public event PropertyChangedEventHandler PropertyChanged;
       public void OnPropertyChanged([CallerMemberName]string prop = "")
       {
           PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
       }
    }
}


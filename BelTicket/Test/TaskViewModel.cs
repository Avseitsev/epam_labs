﻿using Caliburn.Micro;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    public sealed class TaskViewModel : PropertyChangedBase, INotifyPropertyChanged
    {
        private int zaprosi;
        private int duration;
        private int potoci;
        private string url;
        private int end;
        private int onStart;
        private int bandwidth;
        private long maxResponse;
        private long minResponse;
        private long sumResponse;
        private int endGood;
        private int endBad;
        private CancellationTokenSource cancelTokenSource { get; set; }
        private CancellationToken token { get; set; }
        static Mutex mutexObj = new Mutex();
        private string  HTMLstring;


        public int Zaprosi
        {
            get { return zaprosi; }
            set
            {
                zaprosi = value;
                OnPropertyChanged("Zaprosi");
            }
        }
        public int Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                OnPropertyChanged("Duration");
            }
        }
        public int Potoci
        {
            get { return potoci; }
            set
            {
                potoci = value;
                OnPropertyChanged("Potoci");
            }
        }
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                OnPropertyChanged("Title");
            }
        }
        public int End
        {
            get { return end; }
            set
            {              
                end = value;
                OffStat = value + "/" + onStart;
                NameOgYouInt = value * 100 / onStart;
                OnPropertyChanged("End");
            }
        }
        public int OnStart
        {
            get { return onStart; }
            set
            {
                onStart = value;
                OnPropertyChanged("OnStart");
            }
        }

        public int Bandwidth
        {
            get { return bandwidth; }
            set
            {
                bandwidth = value;
                OnPropertyChanged("Bandwidth");
            }
        }

        public long MaxResponse
        {
            get { return maxResponse; }
            set
            {
                maxResponse = value;
                OnPropertyChanged("MaxResponse");
            }
        }
        public long MinResponse
        {
            get { return minResponse; }
            set
            {
                minResponse = value;
                OnPropertyChanged("MinResponse");
            }
        }
        public int EndGood
        {
            get { return endGood; }
            set
            {
                endGood = value;
                OnPropertyChanged("EndGood");
            }
        }
        public int EndBad
        {
            get { return endBad; }
            set
            {
                endBad = value;
                OnPropertyChanged("EndBad");
            }
        }
        public long SumResponse
        {
            get { return sumResponse; }
            set
            {
                Average= Convert.ToInt32(value /endGood);
                sumResponse = value;
                OnPropertyChanged("SumResponse");
            }
        }

        public TaskViewModel()
        {
        }

        private IList<DataPoint> goodPoints;//для графика
        private IList<DataPoint> badPoints;


        public IList<DataPoint> GoodPoints
        {
            get { return goodPoints; }
            set
            {
                goodPoints = value;
                OnPropertyChanged("GoodPoints");
            }
        }

        public IList<DataPoint> BadPoints
        {
            get { return badPoints; }
            set
            {
                badPoints = value;
                OnPropertyChanged("BadPoints");
            }
        }

        public TaskViewModel(int zaprosi,int duration,int potoci,string url)
        {
            HTMLstring = "";//сохраняет точки для графика HTML

          goodPoints = new List<DataPoint> {};
          BadPoints = new List<DataPoint>{};

            this.zaprosi = zaprosi;
            this.duration = duration;
            this.potoci = potoci;
            this.url = url;
            maxResponse = 0;
            maxResponse = 0;
            end = 0;
            EndGood = 0;
            bandwidth = 0;
            EndBad = 0;
            onStart = potoci * duration * zaprosi;
            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;

           Task taskResult4 = Task.Factory.StartNew(() =>//обновляет график каждую секунду
           {
               if (!token.IsCancellationRequested)
               {
                   int time = 0;
                   GoodPoints = new List<DataPoint> { new DataPoint(0, 0)};
                   BadPoints = new List<DataPoint> { new DataPoint(0, 0) };
                   while (End != onStart)
                   {               
                       Thread.Sleep(1000);
                       time++;
                       GoodPoints.Add(new DataPoint(time,EndGood));
                       GoodPoints = new List<DataPoint>(GoodPoints);
                       BadPoints.Add(new DataPoint(time, EndBad));
                       BadPoints = new List<DataPoint>(BadPoints);
                       HTMLstring = HTMLstring + "[\""+time+"\", "+ EndGood+", "+ EndBad+"],";
                   }
               }
           });

            for (int z = 0; z < potoci; z++)// тут делаю потоки
            {
                Task taskResult2 = Task.Factory.StartNew(() =>
                {
                    if (!token.IsCancellationRequested)
                    {
                        for (int i = 0; i < duration; i++)
                        {
                            for (int j = 0; j < zaprosi; j++)
                            {                                
                                Task taskResult3 = Task.Factory.StartNew(async () =>
                                {
                                    if (!token.IsCancellationRequested)
                                    {                                        
                                        try
                                        {
                                            HttpClient client = new HttpClient();
                                            HttpRequestMessage request = new HttpRequestMessage();
                                          //  client.Timeout = TimeSpan.FromMilliseconds(15000);
                                            request.RequestUri = new Uri(this.url);
                                            request.Method = HttpMethod.Get;
                                            request.Headers.Add("Accept", "application/json");
                                            Stopwatch testStopwatch = new Stopwatch();
                                            testStopwatch.Start();
                                            HttpResponseMessage response = await client.SendAsync(request);
                                            ///// эта штука вроде должна мерять поток
                                            var stream = client.GetStreamAsync(this.url).Result;
                                            var bytes = new byte[10000];
                                            Bandwidth = stream.Read(bytes, 0, 10000);//тут вроде она и лежит
                                            stream.Close();
                                            /////
                                            testStopwatch.Stop();

                                            if (MaxResponse < testStopwatch.ElapsedMilliseconds)
                                            {
                                                MaxResponse = testStopwatch.ElapsedMilliseconds;
                                            }
                                            if (MinResponse > testStopwatch.ElapsedMilliseconds || MinResponse == 0)
                                            {
                                                MinResponse = testStopwatch.ElapsedMilliseconds;
                                            }
                                            if (Convert.ToInt32(response.StatusCode) > 299)
                                            {
                                                mutexObj.WaitOne();
                                                EndBad++;
                                                End++;
                                                mutexObj.ReleaseMutex();
                                            }
                                            else
                                            {
                                                mutexObj.WaitOne();
                                                EndGood++;
                                                End++;                                              
                                                mutexObj.ReleaseMutex();
                                                SumResponse = SumResponse + testStopwatch.ElapsedMilliseconds;
                                            }
                                           
                                        }
                                        catch
                                        {
                                            mutexObj.WaitOne();
                                            End++;
                                            EndBad++;
                                            mutexObj.ReleaseMutex();
                                        }
                                        
                                    }
                                });
                            }
                            Thread.Sleep(1000);
                        }
                    }
                });
            }
        }

        public string  OffStat
        {
            get { return End+"/"+OnStart; }
            set
            {
                OnPropertyChanged("OffStat");
            }
        }

        public double NameOgYouInt
        {
            get { return end*100/ OnStart; }
            set
            {
                OnPropertyChanged("NameOgYouInt");
            }
        }

        public int Average
        {
            get {
                if (EndGood != 0)
                {
                    return Convert.ToInt32(SumResponse / EndGood);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                OnPropertyChanged("Average");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public void EndTest()
        {          
            cancelTokenSource.Cancel();
        }

        public void PDF()
        {
            if(End== onStart)
            {
                try
                {
                    Document document = new Document();
                    PdfWriter writer = PdfWriter.GetInstance(document, new FileStream("result.pdf", FileMode.Create));
                    document.Open();
                    PdfPTable table = new PdfPTable(2);

                    PdfPCell cell = new PdfPCell(new Phrase(@"parameters"));
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@" test"));
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Requests per second"));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Zaprosi.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Duration"));
                    table.AddCell(cell);
                   
                    cell = new PdfPCell(new Phrase(Duration.ToString()));
                    table.AddCell(cell);
                   
                    cell = new PdfPCell(new Phrase(@"Amount of threads"));
                    table.AddCell(cell);
                   
                    cell = new PdfPCell(new Phrase(Potoci.ToString()));
                    table.AddCell(cell);
                   
                    cell = new PdfPCell(new Phrase(@"URL"));
                    table.AddCell(cell);
                   
                    cell = new PdfPCell(new Phrase(Url.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"performance"));
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@" test"));
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Total Requests"));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(OnStart.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Max response "));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(MaxResponse.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Min response "));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(MinResponse.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Average response"));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Average.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Failed requests"));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(EndBad.ToString()));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(@"Bandwidth"));
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Bandwidth.ToString()));
                    table.AddCell(cell);

                    document.Add(table);
                    document.Close();
                    writer.Close();


                  //Bitmap screen;
                  //
                  //Size s = new Size(804,404);
                  //
                  //screen = new Bitmap(s.Width, s.Height);
                  //Graphics memoryGraphics = Graphics.FromImage(screen);
                  //
                  //memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
                  //
                  //screen.Save("My.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch
                {

                }
            }
        }

        public void HTML()
        {
            if (End == onStart)
            {
                try
                {
                StreamWriter streamwriter = new StreamWriter(@"reportHTML.html");
                streamwriter.WriteLine("<html>");
                streamwriter.WriteLine("<head>");
                    streamwriter.WriteLine("  <title>HTML-Document</title>");
                    streamwriter.WriteLine("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
                streamwriter.WriteLine("</head>");
                streamwriter.WriteLine("<body>");
                    streamwriter.WriteLine("Requests per second " + Zaprosi + "<br />");
                    streamwriter.WriteLine("Duration " + Duration + "<br />");
                    streamwriter.WriteLine("Amount of threads " + Potoci + "<br />");
                    streamwriter.WriteLine("URL " + Url + "<br />");
                    streamwriter.WriteLine("-----------------------------------------------" + "<br />");
                    streamwriter.WriteLine("Total Requests " + OnStart + "<br />");
                    streamwriter.WriteLine("Max response " + MaxResponse + "<br />");
                    streamwriter.WriteLine("Min response " + MinResponse + "<br />");
                    streamwriter.WriteLine("Average response " + Average + "<br />");
                    streamwriter.WriteLine("Failed requests " + EndBad + "<br />");
                    streamwriter.WriteLine("Bandwidth " + Bandwidth + "<br />");
                    streamwriter.WriteLine("-----------------------------------------------" + "<br />");
                    streamwriter.WriteLine("<div id=\"container\" ></div>");
                    streamwriter.WriteLine("<script src=\"https://cdn.anychart.com/js/latest/anychart-bundle.min.js\" ></script>");
                    streamwriter.WriteLine("<script>");
                        streamwriter.WriteLine("anychart.onDocumentLoad(function() {");
                             streamwriter.WriteLine("var chart = anychart.line()");
                             streamwriter.WriteLine("chart.data({header: [\"#\", \"Successful requests \", \"Failed requests\"],");
                             streamwriter.WriteLine("rows:[");
                             streamwriter.WriteLine(HTMLstring);
                             streamwriter.WriteLine("]});");
                             streamwriter.WriteLine("chart.title(\"AnyChart: Multi - Series Array of Arrays\");");
                             streamwriter.WriteLine("chart.legend(true);");
                             streamwriter.WriteLine("chart.container(\"container\").draw();");
                        streamwriter.WriteLine("});");
                    streamwriter.WriteLine("</script>");
                    streamwriter.WriteLine("<body>");
                streamwriter.WriteLine("</html>");
                streamwriter.Close();
                }
                catch//чисто что бы не спамить на кнопку, а то потоки жалуются
                {
                
                }
            }
        }

    }
}


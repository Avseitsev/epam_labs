﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using BelTicket.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace BelTicket
{
    public class DataInitializer
    {
        public static async void CreateData(IServiceProvider serviceProvider)
        {
            IStorage dataStorage = serviceProvider.GetService<IStorage>();
            var Repository = dataStorage.GetRepository<ItemRepository>();
          if (!Repository.Users.Any())
          {
              Repository.Users.Add(new User("1","Лена", "Полещук", "ru", "ул.Бочкина 153/2", "3034599", "user", "user"));
              Repository.Users.Add(new User("2","Игорь", "Боровиков", "ru", "ул.Бакунина 178", "3452988", "ug", "sd"));
              Repository.Users.Add(new User("3","Илья", "Островский", "en", "ул.Марсиан 5А", "3775654", "ugu", "std"));
              Repository.Users.Add(new User("4","Алена", "Сверчук", "be", "ул.Мальвы 765", "7445633", "admin", "admin"));
              Repository.Users.Add(new User("5","Игорь", "Кановалов", "en", "ул.Сухого 55", "3465571", "admin2", "admin2"));
              Repository.Users.Add(new User("6","Игорь", "Капля", "en", "ул.Водяного 333", "3788754", "1", "1"));
              dataStorage.Save();
            }
 
            if (!Repository.Citys.Any())
            {
                Repository.Citys.Add(new City("Гомель"));
                Repository.Citys.Add(new City("Минск"));
                Repository.Citys.Add(new City("Брест"));
                Repository.Citys.Add(new City("Могилев"));
                Repository.Citys.Add(new City("Гродно"));
                Repository.Citys.Add(new City("Витебск"));
                dataStorage.Save();
            }

            if (!Repository.Venues.Any())
            {

                Repository.Venues.Add(new Venue("Флэт", "ул.Головацкого 19", Repository.Citys.FirstOrDefault(x=>x.Name== "Гомель")));
                Repository.Venues.Add(new Venue("Юность", "площадь Ленина, 2А", Repository.Citys.FirstOrDefault(x => x.Name == "Гомель")));
                Repository.Venues.Add(new Venue("Запад", "ул.Задная 113", Repository.Citys.FirstOrDefault(x => x.Name == "Минск")));
                Repository.Venues.Add(new Venue("Зал славы", "ул.Победы 167", Repository.Citys.FirstOrDefault(x => x.Name == "Минск")));
                Repository.Venues.Add(new Venue("Поющий зал", "ул.Барыкина 87/2", Repository.Citys.FirstOrDefault(x => x.Name == "Брест")));
                Repository.Venues.Add(new Venue("Брест-отель", "площадь победы, 7", Repository.Citys.FirstOrDefault(x => x.Name == "Брест")));
                Repository.Venues.Add(new Venue("Лютня", "ул.Музыкальная, 77", Repository.Citys.FirstOrDefault(x => x.Name == "Могилев")));
                Repository.Venues.Add(new Venue("Пять минут", "ул.Красная, 269/2", Repository.Citys.FirstOrDefault(x => x.Name == "Могилев")));
                Repository.Venues.Add(new Venue("Плаза", "ул.Гагарины, 201", Repository.Citys.FirstOrDefault(x => x.Name == "Гродно")));
                Repository.Venues.Add(new Venue( "Три кота", "ул.Прибрежная, 116", Repository.Citys.FirstOrDefault(x => x.Name == "Гродно")));
                Repository.Venues.Add(new Venue( "Нотка", "ул.Сергеева, 188", Repository.Citys.FirstOrDefault(x => x.Name == "Витебск")));
                Repository.Venues.Add(new Venue( "Батьки", "ул.Киселева, 99", Repository.Citys.FirstOrDefault(x => x.Name == "Витебск")));
                Repository.Venues.Add(new Venue("Подгород", "ул.Ясеная, 144/2", Repository.Citys.FirstOrDefault(x => x.Name == "Витебск")));
                dataStorage.Save();
            }

            if (!Repository.Events.Any())
            {
                Repository.Events.Add(new Events("Шахматный турнир", "13.07.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Флэт"), "1.jpg", "2 тур европейской лиги.16 участников. 80 незабываемых игр. Всем неуснувшим зрителям подарок!"));
                Repository.Events.Add( new Events("Хор им.Ленина", "17.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Юность"), "2.jpg", "Хор Пятницкого Вечер посвящённый 140-летию Ленина"));
                Repository.Events.Add( new Events("Бал", "23.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Запад"), "3.jpg", "Возрождение старой традиции произошло в старинном особняке.Алла и Олег Андрийчук в кругу дистрибьюторов своей организации праздновали дня рождения Аллы.Старинные танцы, фантастические представления, загадочные гадалки и завораживающие романсы.Строгий дресс - код"));
                Repository.Events.Add( new Events("Выставка победы", "24.08.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Зал славы"), "4.jpeg", "История еликой победы в экспонатах"));
                Repository.Events.Add( new Events("Хор им.Ленина", "28.08.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Поющий зал"), "2.jpg", "Хор Пятницкого Вечер посвящённый 140-летию Ленина"));
                Repository.Events.Add( new Events("Звездная ночь", "10.09.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Брест-отель"), "5.jpg", "Горожан всех возрастов приглашают посмотреть на звёзды в телескоп"));
                Repository.Events.Add( new Events("Благотворительный концерт", "15.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Лютня"), "6.jpg", "Сбор средств для ремонта филармонии"));
                Repository.Events.Add( new Events("Выставка истории хмеля", "19.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Пять минут"), "7.jpg", "История.Хмеля. Дигустация достижений хмелевой науки)"));
                Repository.Events.Add( new Events("Дискотека 80-х", "15.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Плаза"), "8.jpg", "Хватит слушать дедово <<в мое время>>. Иди и сам посмотри как было в его время'"));
                Repository.Events.Add( new Events("Званый ужин", "22.08.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Три кота"), "9.jpg", "То самое шоу!"));
                Repository.Events.Add( new Events("Концерт духовых инструментов", "04.07.2017",Repository.Venues.FirstOrDefault(x=>x.Name== "Нотка"), "10.jpg", "ДУХОВОЙ ОРКЕСТР под управлением главного дирижёра Валерия Кравца представит публике грандиозный концерт лучших инструментальных и вокальных произведений с участием солистов оркестра: Григорий Терещенко, Алёна Маринина, Олег Дудко, Юлия Мастич."));
                Repository.Events.Add( new Events("Мясной вечер", "08.06.2017", Repository.Venues.FirstOrDefault(x=>x.Name== "Батьки"), "11.jpg", "Огромные порции мяса по смешным ценам"));
                Repository.Events.Add( new Events("День веганов", "09.06.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Батьки"), "78.jpg", "Топовые салатики по сходным ценам"));
                Repository.Events.Add( new Events("Западный день", "11.06.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Батьки"), "77.jpg", "Топовые блюда запада"));
                Repository.Events.Add( new Events("Фестиваль созвездий", "22.11.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Брест-отель"), "5.jpg", "Лекция о звездах"));
                Repository.Events.Add( new Events("Случайный фильм", "03.06.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Плаза"), "cinema.jpg", "Топовый фильмец"));
                Repository.Events.Add(new Events("Классический концерт", "09.12.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Нотка"), "10.jpg", "Все ваши любимые классические песни"));
                Repository.Events.Add(new Events("Бал-маскарад", "05.06.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Запад"), "3.jpg", "Маска обязательна"));
                Repository.Events.Add(new Events("Зоопарк", "17.06.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Юность"), "72.jpg", "Посмотри на царя зверей!"));
                Repository.Events.Add(new Events("Аукцион", "14.02.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Запад"), "73.jpg", ""));
                Repository.Events.Add(new Events("Цирк дю солей", "14.08.2017", Repository.Venues.FirstOrDefault(x => x.Name == "Запад"), "74.jpg", "Всемирно известный цирк дю солей"));
                Repository.Events.Add(new Events("Театр теней", "19.07.2019", Repository.Venues.FirstOrDefault(x => x.Name == "Подгород"), "75.jpg", "Она всегда с тобой"));
                dataStorage.Save();
            }

            if (!Repository.Tickets.Any())
            {
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Шахматный турнир" && x.Date== "13.07.2017"), "150", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Хор им.Ленина" && x.Date== "17.06.2017"), "250", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Бал" && x.Date== "23.06.2017"), "50", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Выставка победы" && x.Date== "24.08.2017"), "55", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Хор им.Ленина" && x.Date== "28.08.2017"), "70", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Звездная ночь" && x.Date== "10.09.2017"), "75", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Благотворительный концерт" && x.Date== "15.06.2017"), "15", Repository.Users.Find("2")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Выставка истории хмеля" && x.Date== "19.06.2017"), "200", Repository.Users.Find("2")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Дискотека 80-х" && x.Date== "15.06.2017"), "23", Repository.Users.Find("2")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Званый ужин" && x.Date== "22.08.2017"), "123", Repository.Users.Find("2")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Концерт духовых инструментов" && x.Date== "04.07.2017"), "138", Repository.Users.Find("3")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name== "Мясной вечер" && x.Date== "08.06.2017"), "91", Repository.Users.Find("1")));
                Repository.Tickets.Add(new Ticket(Repository.Events.FirstOrDefault(x => x.Name == "Шахматный турнир" && x.Date == "13.07.2017"), "137", Repository.Users.Find("4")));

                dataStorage.Save();
            }

            if (!Repository.Orders.Any())
            {
                Repository.Orders.Add(new Order(Repository.Tickets.FirstOrDefault(x=>x.Price== "150"), "В ожидании", Repository.Users.Find("4"), "trackNO"));
                Repository.Orders.Add(new Order(Repository.Tickets.FirstOrDefault(x=>x.Price== "150"), "В ожидании", Repository.Users.Find("3"), "trackNO"));
                Repository.Orders.Add(new Order(Repository.Tickets.FirstOrDefault(x => x.Price == "70"), "В ожидании", Repository.Users.Find("5"), "trackNO"));
                Repository.Orders.Add(new Order(Repository.Tickets.FirstOrDefault(x => x.Price == "123"), "В ожидании", Repository.Users.Find("6"), "trackNO"));
                Repository.Orders.Add(new Order(Repository.Tickets.FirstOrDefault(x=>x.Price=="55"), "В ожидании", Repository.Users.Find("2"), "trackNO"));               
                dataStorage.Save();
            }
            if (!Repository.Role.Any())
            {
                Repository.Role.Add(new Role("Admin"));
                Repository.Role.Add(new Role("User"));
                dataStorage.Save();
            }
            
             if (!Repository.RoleStatusOfUser.Any())
            {
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name=="Admin"), Repository.Users.Find("4")));
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name== "Admin"), Repository.Users.Find("5")));
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name=="User"), Repository.Users.Find("1")));
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name=="User"), Repository.Users.Find("2")));
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name=="User"), Repository.Users.Find("3")));
                Repository.RoleStatusOfUser.Add(new RoleStatusOfUser(Repository.Role.FirstOrDefault(x=>x.Name== "User"), Repository.Users.Find("6")));
                dataStorage.Save();
            }
        }
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
using BelTicket.Models;

namespace AspNetCoreCsvImportExport.Formatters
{
    /// <summary>
    /// Original code taken from
    /// http://www.tugberkugurlu.com/archive/creating-custom-csvmediatypeformatter-in-asp-net-web-api-for-comma-separated-values-csv-format
    /// Adapted for ASP.NET Core and uses ; instead of , for delimiters
    /// </summary>
    public class CsvOutputFormatter :  OutputFormatter
    {
        private readonly CsvFormatterOptions _options;

        public string ContentType { get; private set; }

       public CsvOutputFormatter(CsvFormatterOptions csvFormatterOptions)
       {
           ContentType = "text/csv";
           SupportedMediaTypes.Add(Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/csv"));
      
           if (csvFormatterOptions == null)
           {
               throw new ArgumentNullException(nameof(csvFormatterOptions));
           }
      
           _options = csvFormatterOptions;
      
       }


        public async override Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {        
            //получил запрос
            var response = context.HttpContext.Response;
            Type type = context.Object.GetType();
            Type itemType;
            
            //выдернул тип класса
            if (type.GetGenericArguments().Length > 0)
            {
                itemType = type.GetGenericArguments()[0];
            }
            else
            {
                itemType = type.GetElementType();
            }

            //сюда потом накапливать csv
            StringWriter _stringWriter = new StringWriter();


            //как бы оздаю названия для столбцов
            if (_options.UseSingleLineHeaderInCsv)
            {
                _stringWriter.WriteLine(
                    string.Join<string>(
                        _options.CsvDelimiter, itemType.GetProperties().Select(x => x.Name)
                    )
                );
            }

            
                //проверяю какой класс
             if (itemType==new City().GetType())
             {
               foreach (var obj in (IEnumerable<object>)context.Object)
               {
               
                   //разбиение что бы потом смотреть какие там лежат значения
                   var vals = obj.GetType().GetProperties().Select(
                       pi => new {
                           Value = pi.GetValue(obj)
                       }
                   );
               
                   string _valueLine = string.Empty;
               
                   foreach (var val in vals)
                   {
               
                       if (val.Value != null)
                       {
               
                           var _val = val.Value.ToString();
               
                           //Check if the value contans a comma and place it in quotes if so
                           if (_val.Contains(","))
                               _val = string.Concat("\"", _val, "\"");
               
                           //Replace any \r or \n special characters from a new line with a space
                           if (_val.Contains("\r"))
                               _val = _val.Replace("\r", " ");
                           if (_val.Contains("\n"))
                               _val = _val.Replace("\n", " ");
               
                           _valueLine = string.Concat(_valueLine, _val, _options.CsvDelimiter);
               
                       }
                       else
                       {
                           _valueLine = string.Concat(_valueLine, string.Empty, _options.CsvDelimiter);
                       }
                   }
               
                   _stringWriter.WriteLine(_valueLine.TrimEnd(_options.CsvDelimiter.ToCharArray()));
               }
             }

             if (itemType == new Venue().GetType())
             {
              IEnumerable<Venue> Enum = (IEnumerable<Venue>)context.Object;

                int i = 0;

                foreach (var obj in Enum)
                {

                    //разбиение что бы потом смотреть какие там лежат значения
                    var vals = obj.GetType().GetProperties().Select(
                        pi => new {
                            Value = pi.GetValue(obj)
                        }
                    );

                    string _valueLine = string.Empty;
                   
                    foreach (var val in vals)
                    {
                       
                        if (val.Value != null)
                        {

                            var _val = val.Value.ToString();
                            if (_val== new City().GetType().ToString())//проверка на поле сити
                            {
                                _val = Enum.Skip(i).Take(1).Single().Name;
                                i++;
                            }

                            //Check if the value contans a comma and place it in quotes if so
                            if (_val.Contains(","))
                                _val = string.Concat("\"", _val, "\"");

                            //Replace any \r or \n special characters from a new line with a space
                            if (_val.Contains("\r"))
                                _val = _val.Replace("\r", " ");
                            if (_val.Contains("\n"))
                                _val = _val.Replace("\n", " ");

                            _valueLine = string.Concat(_valueLine, _val, _options.CsvDelimiter);

                        }
                        else
                        {
                            _valueLine = string.Concat(_valueLine, string.Empty, _options.CsvDelimiter);
                        }
                       
                    }

                    _stringWriter.WriteLine(_valueLine.TrimEnd(_options.CsvDelimiter.ToCharArray()));
                }
            }

             if (itemType == new Events().GetType())
             {
                IEnumerable<Events> Enum = (IEnumerable<Events>)context.Object;

                int i = 0;

                foreach (var obj in Enum)
                {

                    //разбиение что бы потом смотреть какие там лежат значения
                    var vals = obj.GetType().GetProperties().Select(
                        pi => new {
                            Value = pi.GetValue(obj)
                        }
                    );

                    string _valueLine = string.Empty;

                    foreach (var val in vals)
                    {

                        if (val.Value != null)
                        {

                            var _val = val.Value.ToString();
                            if (_val == new Venue().GetType().ToString())//проверка на поле сити
                            {
                                _val = Enum.Skip(i).Take(1).Single().Venue.Name;
                                i++;
                            }

                            //Check if the value contans a comma and place it in quotes if so
                            if (_val.Contains(","))
                                _val = string.Concat("\"", _val, "\"");

                            //Replace any \r or \n special characters from a new line with a space
                            if (_val.Contains("\r"))
                                _val = _val.Replace("\r", " ");
                            if (_val.Contains("\n"))
                                _val = _val.Replace("\n", " ");

                            _valueLine = string.Concat(_valueLine, _val, _options.CsvDelimiter);

                        }
                        else
                        {
                            _valueLine = string.Concat(_valueLine, string.Empty, _options.CsvDelimiter);
                        }

                    }

                    _stringWriter.WriteLine(_valueLine.TrimEnd(_options.CsvDelimiter.ToCharArray()));
                }
            }

           
            var streamWriter = new StreamWriter(response.Body);
            await streamWriter.WriteAsync(_stringWriter.ToString());
            await streamWriter.FlushAsync();
        }
    }
}

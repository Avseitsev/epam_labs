﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface RoleStatusOfUserRepository : IRepository
    {
        IEnumerable<RoleStatusOfUser> AllRoleStatusOfUser();
        RoleStatusOfUser RoleByUser(User user);
        void UpdataRole(RoleStatusOfUser roleStatusOfUser);
    }
}

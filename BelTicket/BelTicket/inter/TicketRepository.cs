﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface TicketRepository : IRepository
    {
        IEnumerable<Ticket> TicketByIdEvent(int idEvent);
        IEnumerable<Ticket> TicketByIdEventStatus(int idEvent);
        void StatusNoSell(Ticket ticket);
        Ticket TicketById(int idTicket);      
        void TicketAdd(Ticket ticket);
        IEnumerable<Ticket> TicketByIdSeller(string idSeller);
        IEnumerable<Ticket> TicketExplosed(string idSeller);
        void TicketDelFull(int id, User Out);
    }
}

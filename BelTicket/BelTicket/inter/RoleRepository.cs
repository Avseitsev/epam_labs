﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface RoleRepository : IRepository
    {
        IEnumerable<Role> AllRole();
    }
}

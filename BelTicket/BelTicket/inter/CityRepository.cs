﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface CityRepository : IRepository
    {
        IEnumerable<City> CityAll();
        IEnumerable<City> AllForApi(string name);
        bool UniquenessCity(String name);
        void CityAdd(String name);
        void ChangeCity(String name, String newName);
        bool DelCity(String name);
        City CityByName(String name);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface ChatRepository : IRepository
    {
        IEnumerable<Chat> ChatByUserId(string id);
        void ChatAdd(Chat chat);
        void ChatAvtoDel (int Orderid,User Out);
        void ChatAddForBuy(int idOrder, string message, User Out);
    }
}

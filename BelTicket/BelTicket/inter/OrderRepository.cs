﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface OrderRepository : IRepository
    {
        IEnumerable<Order> AllForIdUser(string idUser);
        IEnumerable<Order> AllForIdUserBay(string idUser);
        Order OrderById(int id);
        void OrderDel(int id);
        void OrderStatusSell(int id);
        void OrderDellStatusNoSell(int id);
        bool UniqueOrder(User user, Ticket ticket);
        IEnumerable<Order> AllOrder();
        void NewOrder(Order order);
    }
}

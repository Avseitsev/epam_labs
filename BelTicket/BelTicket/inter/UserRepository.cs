﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface UserRepository : IRepository
    {
        IEnumerable<User> UserAll();
        IEnumerable<User> UserById(string id);
        IEnumerable<User> UserByName(string Name);
        bool UniquenessUser(User user);
        void UserUpdate(User user);
        void AddUserThisRole(User user);
    }
}

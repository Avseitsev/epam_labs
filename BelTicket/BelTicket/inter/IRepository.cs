﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.inter
{
    public interface IRepository
    {
        void SetStorageContext(IStorageContext storageContext);
    }
}

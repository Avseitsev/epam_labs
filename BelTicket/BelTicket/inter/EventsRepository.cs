﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    public interface EventsRepository : IRepository
    {
        IEnumerable<Events> All();
        int len();
        IEnumerable<Events> Nyne();
        IEnumerable<Events> ForPage(int Page);
        IEnumerable<Events> AllForApi(string city, string data, string dataTo, string name, string venue);
        Events EventsByInfo(string name, string data, Venue venue);
        void EventsUpdate(Events events, Events newEvents);
       bool EventsDel(Events events);
        void EventsAdd(Events events);
    }
}

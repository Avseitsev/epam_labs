﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.Models;

namespace BelTicket.inter
{
    interface VenueRepository : IRepository
    {
        IEnumerable<Venue> VenueAll();
        IEnumerable<Venue> AllForApi(string name,string address,string city);
        bool UniquenesVenue(String name, String address, String cityName);
        void VenueUpdate(Venue venue, string newName, string newAddress, string newCityName);
        Venue VenueFind(String name, String address, String cityName);
        void VenueAdd(Venue venue);
        bool VenueDel(Venue venue);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using BelTicket.Models;
using Microsoft.EntityFrameworkCore;

namespace BelTicket
{
    public class ItemRepository : EventsRepository, TicketRepository, UserRepository, OrderRepository,RoleRepository, RoleStatusOfUserRepository, CityRepository, VenueRepository, ChatRepository
    {
        public StorageContext storageContext;
        public DbSet<User> Users { get; set; }
        public DbSet<City> Citys { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleStatusOfUser> RoleStatusOfUser { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Chat> Chat { get; set; }

        public ItemRepository()
        {
        }

        public void SetStorageContext(IStorageContext storageContext)
        {
            this.storageContext = storageContext as StorageContext;
            this.Citys = this.storageContext.Set<City>();
            this.Venues = this.storageContext.Set<Venue>();
            Venues
                .Include(c => c.City)
                .Load();
            this.Users= this.storageContext.Set<User>();
            this.Events = this.storageContext.Set<Events>();
            Events
                .Include(c => c.Venue)
                 .Load();
            this.Tickets = this.storageContext.Set<Ticket>();
            Tickets
                .Include(c => c.Events)
                .Include(c => c.Seller)
                .Load();
            this.Orders = this.storageContext.Set<Order>();
            Orders
                .Include(c => c.Buyer)
                .Include(c => c.Ticket)
                .Load();
            this.Role = this.storageContext.Set<Role>();
            this.RoleStatusOfUser = this.storageContext.Set<RoleStatusOfUser>();
            RoleStatusOfUser
                .Include(c => c.Role)
                .Include(c => c.User)
                .Load();
            this.Chat = this.storageContext.Set<Chat>();
            Chat
                .Include(c=>c.In)
                .Include(c => c.Out)
                .Load();
        }

        public IEnumerable<Events> All()
        {
            return Events.OrderBy(x=>x.Name).ToList();
        }

        public IEnumerable<Ticket> TicketByIdEvent(int idEvent)
        {
            return Tickets.Where(x => x.Events.Id == idEvent).OrderBy(x => x.Price).ToList();

          //  SqlParameter param = new SqlParameter("@name", idEvent);
          //  return storageContext.Tickets.FromSql("SELECT * FROM Tickets WHERE EventsId LIKE @name", param).ToList();
        }

        public IEnumerable<User> UserAll()
        {
            return Users.ToList();
        }

        public IEnumerable<User> UserById(string id)
        {
            return Users.Where(x => x.Id == id).ToList();

          //  SqlParameter param = new SqlParameter("@name", id);
          //  return storageContext.Users.FromSql("SELECT * FROM AspNetUsers WHERE Id LIKE @name", param).ToList();
        }

        public IEnumerable<User> UserByName(string Name)
        {
           try
           {
                return Users.Where(x => x.UserName == Name).ToList();

               // SqlParameter param = new SqlParameter("@name", Name);
               // return storageContext.Users.FromSql("SELECT * FROM AspNetUsers WHERE UserName LIKE @name", param).ToList();
           }
            catch (System.Data.SqlClient.SqlException)
            {
                return null;
            }
           
        }

        public IEnumerable<Order> AllForIdUser(string idUser)
        {
            return Orders.Where(x => x.Ticket.Seller.Id == idUser).ToList();

           // SqlParameter param = new SqlParameter("@name", idUser);
           // return storageContext.Orders.FromSql("SELECT * FROM Orders WHERE BuyerId LIKE @name", param).ToList();
        }

        public IEnumerable<Order> AllForIdUserBay(string idUser)
        {
            return Orders.Where(x => x.Buyer.Id == idUser).ToList();           
        }

        public IEnumerable<Order> AllOrder()
        {
            return Orders.ToList();
        }

        public IEnumerable<Role> AllRole()
        {
            return Role.ToList();
        }

        public IEnumerable<RoleStatusOfUser> AllRoleStatusOfUser()
        {
            return RoleStatusOfUser.ToList();
        }

        public IEnumerable<Ticket> TicketByIdSeller(string idSeller)
        {
            return Tickets.Where(x => x.Seller.Id == idSeller).ToList();

           // SqlParameter param = new SqlParameter("@name", idSeller);
           // return storageContext.Tickets.FromSql("SELECT * FROM Tickets WHERE SellerId LIKE @name", param).ToList();
        }

        public bool UniquenessUser(User user)
        {
            if (Users.Where(x => x.UserName == user.UserName).ToList().Count>0)
            {
                return false;
            }
            return true;
        }

        public RoleStatusOfUser RoleByUser(User user)
        {
           return RoleStatusOfUser.FirstOrDefault(x=>x.User.Id == user.Id);
        }

        public Ticket TicketById(int idTicket)
        {
            return Tickets.FirstOrDefault(x => x.Id == idTicket);
        }

        public bool UniqueOrder(User user, Ticket ticket)
        {
            if (Orders.Where(x => x.Ticket == ticket && x.Buyer == user).Count()>0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void NewOrder(Order order)
        {
            storageContext.Entry(order).State = EntityState.Added;
            storageContext.SaveChanges();
        }

        public IEnumerable<City> CityAll()
        {
            return Citys.OrderBy(x => x.Name).ToList();
        }

        public void UpdataRole(RoleStatusOfUser roleStatusOfUser)
        {
            RoleStatusOfUser RSOU = roleStatusOfUser;
            if (RSOU.Role.Name== "User")
            {
                RSOU.Role=Role.Find(1);
            }
            else
            {
                RSOU.Role= Role.Find(2);
            }
            storageContext.Entry(RSOU).State = EntityState.Modified;
            storageContext.SaveChanges();
        }
        public void UserUpdate(User user)
        {
            storageContext.Users.Update(user);
            storageContext.SaveChanges();
        }

        public void AddUserThisRole(User user)
        {
            storageContext.Users.Add(user);
           storageContext.RoleStatusOfUser.Add(new RoleStatusOfUser(storageContext.Role.Find(2),user));
           storageContext.SaveChanges();
        }

        public bool UniquenessCity(string name)
        {
            if (Citys.Where(x => x.Name == name).Count() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void CityAdd(string name)
        {
            storageContext.Citys.Add(new City(name));
            storageContext.SaveChanges();
        }

        public void ChangeCity(string name, string newName)
        {
            City c = CityByName(name);
            c.Name = newName;
            storageContext.Entry(c).State = EntityState.Modified;
            storageContext.SaveChanges();
        }

        public City CityByName(string name)
        {
            return Citys.FirstOrDefault(x => x.Name == name);
        }

        public bool DelCity(string name)
        {
            if (Venues.FirstOrDefault(x => x.City.Name == name) == null)
            {
                Citys.Remove(Citys.FirstOrDefault(x => x.Name == name));
                storageContext.SaveChanges();
                return true;
            }
            return false;
        }

        public IEnumerable<Venue> VenueAll()
        {
            return Venues.OrderBy(x => x.Name).ToList();
        }

        public bool UniquenesVenue(String name, String address, String cityName)
        {
            if (Venues.Where(x => x.Name == name && x.Address == address && x.City.Name==cityName).Count() > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void VenueUpdate(Venue venue, string newName, string newAddress, string newCityName)
        {
            venue.Name = newName;
            venue.Address = newAddress;
            venue.City = CityByName(newCityName);
            storageContext.Entry(venue).State = EntityState.Modified;
            storageContext.SaveChanges();
        }

        public Venue VenueFind(string name, string address, string cityName)
        {
            return Venues.FirstOrDefault(x=>x.Name==name && x.Address == address && x.City.Name==cityName);
        }

        public void VenueAdd(Venue venue)
        {
            storageContext.Entry(venue).State = EntityState.Added;
            storageContext.SaveChanges();
        }

        public bool VenueDel(Venue venue)
        {
            if (Events.FirstOrDefault(x => x.Venue == venue) == null)
            {
                Venues.Remove(venue);
                storageContext.SaveChanges();
                return true;
            }
            return false;

        }

        public Events EventsByInfo(string name, string data, Venue venue)
        {
            return Events.FirstOrDefault(x => x.Venue == venue && x.Name==name && x.Date==data);
        }


        public void EventsUpdate(Events events, Events newEvents)
        {
            events.Date = newEvents.Date;
            events.Description = newEvents.Description;
            events.Image = newEvents.Image;
            events.Name = newEvents.Name;
            events.Venue = newEvents.Venue;
            storageContext.Entry(events).State = EntityState.Modified;
            storageContext.SaveChanges();
        }

        public bool EventsDel(Events events)
        {
            if (TicketByIdEvent(events.Id).Count() > 0)
            {


                return false;
            }
            else
            {
                Events.Remove(events);
                storageContext.SaveChanges();
                return true;
            }
        }

        public void EventsAdd(Events events)
        {
            storageContext.Entry(events).State = EntityState.Added;
            storageContext.SaveChanges();
        }

        public void TicketAdd(Ticket ticket)
        {
            storageContext.Entry(ticket).State = EntityState.Added;
            storageContext.SaveChanges();
        }

        public IEnumerable<Ticket> TicketByIdEventStatus(int idEvent)
        {
            return Tickets.Where(x => x.Events.Id == idEvent && x.Status== "Продается").OrderBy(x => x.Price).ToList();
        }

        public Order OrderById(int id)
        {
            return Orders.FirstOrDefault(x=>x.Id==id);
        }

        public void StatusNoSell(Ticket ticket)
        {
            ticket.Status = "Продан";
            storageContext.Entry(ticket).State = EntityState.Modified;
            storageContext.SaveChanges();
        }

        public void OrderDel(int id)
        {
            Orders.Remove(Orders.FirstOrDefault(x => x.Id == id));
            storageContext.SaveChanges();
        }

        public void OrderStatusSell(int id)
        {
            Order r = Orders.FirstOrDefault(x => x.Id == id);
            r.Status = "куплен";
            storageContext.Entry(r).State = EntityState.Modified;
            storageContext.SaveChanges();
        }

        public void OrderDellStatusNoSell(int id)
        {
            Order r = Orders.FirstOrDefault(x => x.Id == id);
            List<Order> list = Orders.Where(x => x.Ticket.Id == r.Ticket.Id && x.Status== "В ожидании").ToList();
            foreach (Order item in list)
            {
                Orders.Remove(item);
                storageContext.SaveChanges();
            }
        }

        public IEnumerable<Ticket> TicketExplosed(string idSeller)
        {
            return Tickets.Where(x => x.Seller.Id == idSeller && x.Status== "Продается").ToList();
        }

        public IEnumerable<Chat> ChatByUserId(string id)
        {
            return Chat.Where(x=>x.In.Id==id).OrderByDescending(x => x.Time);
        }

        public void ChatAdd(Chat chat)
        {
            storageContext.Entry(chat).State = EntityState.Added;
            storageContext.SaveChanges();
        }

        public void ChatAvtoDel(int id, User Out)
        {
            Order r = Orders.FirstOrDefault(x => x.Id == id);
            List<Order> list = Orders.Where(x => x.Ticket.Id == r.Ticket.Id && x.Status == "В ожидании").ToList();
            foreach (Order item in list)
            {
                string s = "Ваш заказ на билет <<"+item.Ticket.Events.Name+">> автоматически отклонен, так как билет уже продан";
                ChatAdd(new Chat(s, item.Buyer, Out, DateTime.Now));
            }
        }

        public void ChatAddForBuy(int idOrder, string message, User Out)
        {
            Order r = Orders.FirstOrDefault(x => x.Id == idOrder);
            message="Ваш заказ на "+r.Ticket.Events.Name+message;
            ChatAdd(new Chat(message, r.Buyer, Out, DateTime.Now));

        }

        public void TicketDelFull(int id, User Out)
        {
            Ticket t = Tickets.FirstOrDefault(x=>x.Id==id);
            List<Order> list = Orders.Where(x => x.Ticket.Id == id).ToList();
            foreach (Order item in list)
            {
                string s = "Извините, но билет был снят владельцем с продажи, поэтмоу ваш запрос на покупку "+item.Ticket.Events.Name+" автоматически отменен!";
                ChatAdd(new Chat(s, item.Buyer, Out, DateTime.Now));
                Orders.Remove(item);
                storageContext.SaveChanges();
            }
                Tickets.Remove(t);
            storageContext.SaveChanges();
        }

        public IEnumerable<Events> AllForApi(string city, string data, string dataTo, string name, string venue)
        {
            List<Events> l = Events.ToList();

            if (data != null)
            {
                try//он тут чисто на случай если введут бред в свагере, ттак суда 100% никак не попадет неадекватная дата
                {
                    l = l.Where(x => DateTime.ParseExact(x.Date, "dd.MM.yyyy", null) >= DateTime.ParseExact(data, "dd.MM.yyyy", null) || x.Date == data).ToList();
                }
                catch
                {

                }
            }

            if (dataTo != null)
            {
                try//он тут чисто на случай если введут бред в свагере, ттак суда 100% никак не попадет неадекватная дата
                {
                    l = l.Where(x => DateTime.ParseExact(x.Date, "dd.MM.yyyy", null) <= DateTime.ParseExact(dataTo, "dd.MM.yyyy", null) || x.Date == data).ToList();
                  }
                      catch
                  {

                  }
        }

            if (name != null)
            {
                l = l.Where(x => x.Name.StartsWith(name) || x.Name==name).ToList();
            }

            if (city != null)
            {
                l = l.Where(x => x.Venue.City.Name.StartsWith(city) || x.Venue.City.Name==city).ToList();
            }

            if (venue != null)
            {
                l = l.Where(x => x.Venue.Name.StartsWith(venue) || x.Venue.Name == venue).ToList();
            }
            return l.OrderBy(x=>x.Name);
        }

        public IEnumerable<City> AllForApi(string name)
        {
            List<City> l = Citys.ToList();
            if (name != null)
            {
                l = l.Where(x => x.Name.StartsWith(name) || x.Name == name).ToList();
            }
            return l.OrderBy(x => x.Name);
        }

        public IEnumerable<Venue> AllForApi(string name, string address, string city)
        {
            List<Venue> l = Venues.ToList();
            if (name != null)
            {
                l = l.Where(x => x.Name.StartsWith(name) || x.Name == name).ToList();
            }

            if (address != null)
            {
                l = l.Where(x => x.Address.StartsWith(address) || x.Address == address).ToList();
            }

            if (city != null)
            {
                l = l.Where(x => x.City.Name.StartsWith(city) || x.City.Name == city).ToList();
            }

            return l.OrderBy(x => x.Name);
        }

        public IEnumerable<Events> Nyne()
        {
            if (Events.Count() < 9)
            {
                return Events.OrderBy(x => x.Name).ToList();
            }
            else
            {
                return Events.Take(9).OrderBy(x => x.Name).ToList();
            }
        }

        public int len()
        {
            return Events.Count();
        }

        public IEnumerable<Events> ForPage(int Page)
        {
            return Events.OrderBy(x => x.Name).Skip(Page*9).Take(9).ToList();
        }
    }
}

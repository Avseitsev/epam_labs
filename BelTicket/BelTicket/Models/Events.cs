﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class Events
    {
        public Events(int id, string name, string date, Venue venue, string image, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Date = date;
            this.Venue = venue;
            this.Image = image;
            this.Description = description;
        }
        public Events(string name, string date, Venue venue, string image, string description)
        {
            this.Name = name;
            this.Date = date;
            this.Venue = venue;
            this.Image = image;
            this.Description = description;
        }

        public Events()
        { }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Date { get; set; }
 
        public Venue Venue { get; set; }

        public string Image { get; set; }

        public string Description { get; set; }
    }
}

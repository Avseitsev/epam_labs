﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class Venue//место
    {
        public Venue(string name, string address, City city)
        {
            this.Name = name;
            this.Address = address;
            this.City = city;
        }

        public Venue()
        { }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public City City { get; set; }
    }
}

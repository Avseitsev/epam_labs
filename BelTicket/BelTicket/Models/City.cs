﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class City
    {
        public City(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public City(string name)
        {
            this.Name = name;
        }

        public City()
        { }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}

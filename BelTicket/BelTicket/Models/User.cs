﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace BelTicket.Models
{
    public class User : IdentityUser
    {
        public User(string id,string firstName, string lastName, string localization, string address, string phoneNumber, string userName, string passwordHash)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Localization = localization;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
            this.UserName = userName;
            this.PasswordHash = passwordHash;
        }
        public User(string firstName, string lastName, string localization, string address, string phoneNumber, string userName, string passwordHash)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Localization = localization;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
            this.UserName = userName;
            this.PasswordHash = passwordHash;
        }

        public User()
        { }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Localization { get; set; }

        public string Address { get; set; }

    }
}

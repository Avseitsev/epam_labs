﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class Order
    {
        public Order(Ticket ticket, string status, User buyer, string trackNO)
        {
            this.Ticket = ticket;
            this.Status = status;
            this.Buyer = buyer;
            this.TrackNO = trackNO;
        }

        public Order()
        { }

        public int Id { get; set; }

        public Ticket Ticket { get; set; }

        public string Status { get; set; }

        public User Buyer { get; set; }

        public string TrackNO { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class Ticket
    {
        public Ticket(Events events, string price, User seller)
        {
            this.Events = events;
            this.Price = price;
            this.Seller = seller;
            Status = "Продается";
        }

        public Ticket()
        { }

        public int Id { get; set; }

        public Events Events { get; set; }

        public string Price { get; set; }

        public User Seller { get; set; }

        public string Status { get; set; }
    }
}

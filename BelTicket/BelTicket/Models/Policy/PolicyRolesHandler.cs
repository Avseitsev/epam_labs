﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using Microsoft.AspNetCore.Authorization;

namespace BelTicket.Models.Policy
{
    public class PolicyRolesHandler : AuthorizationHandler<PolicyRoles>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PolicyRoles requirement)
        {
          IStorage storage=new Storage();
        string idUser = "-1";

            foreach (User item in storage.GetRepository<UserRepository>().UserAll().ToList())
            {
                if (context.User.Identity.Name == item.UserName)
                {
                    idUser = item.Id;
                }
            }

             int idRole = -1;
             foreach (Role item in storage.GetRepository<RoleRepository>().AllRole().ToList())
             {
                if (item.Name == requirement.Role)
                {
                    idRole = item.Id;
                }
             }

            if (idRole!=-1 && idUser!="-1")
            {
                foreach (RoleStatusOfUser item in storage.GetRepository<RoleStatusOfUserRepository>().AllRoleStatusOfUser().ToList())
                {
                    if (item.Role.Id == idRole && item.User.Id == idUser)
                    {
                        context.Succeed(requirement);
                    }
                }
            }
            return Task.CompletedTask;
        }
    }
}

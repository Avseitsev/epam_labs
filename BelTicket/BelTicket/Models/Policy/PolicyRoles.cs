﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace BelTicket.Models.Policy
{
    public class PolicyRoles : IAuthorizationRequirement
    {
        public string Role { get; set; }
        public PolicyRoles(string role)
        {
           Role = role;
        }
    }   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class RoleStatusOfUser
    {
        public RoleStatusOfUser(Role role,User user)
        {
            this.Role = role;
            this.User = user;
        }

        public RoleStatusOfUser()
        { }

        public int Id { get; set; }

        public Role Role { get; set; }

        public User User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models
{
    public class Chat
    {
        public Chat(string Message, User In, User Out, DateTime Time)
        {
            this.Message = Message;
            this.In = In;
            this.Out = Out;
            this.Time = Time;
        }

        public Chat()
        { }

        public int Id { get; set; }

        public string Message { get; set; }
        public User In { get; set; }
        public User Out { get; set; }
        public DateTime Time { get; set; }
    }
}

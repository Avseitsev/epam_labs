﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace BelTicket.Models
{
    public class Role
    {
        public Role(string name)
        {
            this.Name = name;
        }

        public Role()
        { }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}

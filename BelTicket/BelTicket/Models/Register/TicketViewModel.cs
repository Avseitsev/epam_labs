﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace BelTicket.Models.Register
{
    public class TicketViewModel
    {
        public string Date { get; set; }
        public string Name { get; set; }

        public string Venue { get; set; }
        public string VenueCity { get; set; }
        public string VenueAddress { get; set; }

        public string Price { get; set; }
    }
}

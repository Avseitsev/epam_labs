﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelTicket.Models.Register
{
    public class UserViewModel
    {
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Localization { get; set; }
        public string UserName { get; set; }
    }
}

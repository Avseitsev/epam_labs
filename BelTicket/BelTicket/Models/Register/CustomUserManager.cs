﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BelTicket.Models.Register
{
    public class CustomUserManager : UserManager<User>
    {
        public CustomUserManager(IUserStore<User> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<User> passwordHasher, IEnumerable<IUserValidator<User>> userValidators, IEnumerable<IPasswordValidator<User>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<User>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            IStorage storage = new Storage();

            foreach(User item in storage.GetRepository<UserRepository>().UserAll().ToList())
            {
               store.CreateAsync(item,CancellationToken);
            }
        }

        public override Task<User> FindByNameAsync(string name)
        {

            return Store.FindByNameAsync(name, CancellationToken);
        }

        public override Task<bool> CheckPasswordAsync(User user, string Password)
        {
            if(user!=null)
            return Task<bool>.Factory.StartNew(() => user.PasswordHash == Password);
            else
                return Task<bool>.Factory.StartNew(() => false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace BelTicket.Models.Register
{
    public class EventViewModel
    {
        public string OldName { get; set; }
        public string OldDate { get; set; }
        public string OldVenue { get; set; }
        public string OldVenueCity { get; set; }
        public string OldVenueAddress { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Venue { get; set; }
        public string VenueCity { get; set; }
        public string VenueAddress { get; set; }
        public string Description { get; set; }
        public IFormFile Photo { get; set; }
    }
}

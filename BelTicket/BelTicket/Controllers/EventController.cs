﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using BelTicket.Models;
using BelTicket.Models.Register;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BelTicket.Controllers
{
    [Route("api/[controller]")]
    public class EventController : Controller
    {

        private CustomUserManager customUserManager;
        private SignInManager<User> signInManager;
        private IStorage storage;
        private IHostingEnvironment environment;

        public EventController(IHostingEnvironment environment, IStorage storage, CustomUserManager customUserManage, SignInManager<User> signInManager)
        {
            this.signInManager = signInManager;
            this.customUserManager = customUserManage;
            this.storage = storage;
            this.environment = environment;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Events> Get(string city, string data,string dataTo, string name, string venue)
        {
            if (data != null)
            {
                try//он тут чисто на случай если введут бред в свагере, ттак суда 100% никак не попадет неадекватная дата
                {
                    string[] result = Regex.Split(data, "-");
                    string[] result2 = Regex.Split(data, "-");
                    result2[0] = result[2];
                    result2[2] = result[0];
                    result2[0] = result2[0] + '.' + result2[1] + '.' + result2[2];
                    data = result2[0];
                }
                catch {

                }
            }

            if (dataTo != null)
            {
                try//он тут чисто на случай если введут бред в свагере, ттак суда 100% никак не попадет неадекватная дата
                {
                    string[] result = Regex.Split(dataTo, "-");
                    string[] result2 = Regex.Split(dataTo, "-");
                    result2[0] = result[2];
                    result2[2] = result[0];
                    result2[0] = result2[0] + '.' + result2[1] + '.' + result2[2];
                    dataTo = result2[0];
                }
                catch
                {

                }
            }
            return this.storage.GetRepository<EventsRepository>().AllForApi(city, data,dataTo, name,venue).ToList();
        }

        // GET api/values/5
        [HttpGet("{sPage}")]
        public IEnumerable<Events> Get(string sPage)
        {
            int page = 0;

            try
            {
                page= Convert.ToInt32(sPage);
            }
            catch (FormatException)
            {
                page = 1;
            }
            catch (OverflowException)
            {
                page = 1;
            }
            return this.storage.GetRepository<EventsRepository>().ForPage(page);
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using BelTicket.inter;
using BelTicket.Models;
using BelTicket.Models.Register;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace BelTicket.Controllers
{
    public class HomeController : Controller
    {
        private CustomUserManager customUserManager;
        private SignInManager<User> signInManager;
        private IStorage storage;
        private IHostingEnvironment environment;

        public HomeController(IHostingEnvironment environment, IStorage storage, CustomUserManager customUserManage, SignInManager<User> signInManager)
        {
            this.signInManager = signInManager;
            this.customUserManager = customUserManage;
            this.storage = storage;
            this.environment = environment;
        }

        [Authorize]
        public void IndexLog()
        {
           string cultureName = ThisUser(User.Identity.Name).Localization;
           Response.Cookies.Append(
               CookieRequestCultureProvider.DefaultCookieName,
               CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(cultureName)),
               new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
               );
            Response.Redirect("/Home/Index");
        }

        public ActionResult Index()
        {
            ViewData["ReturnUrl"] = null;
            ViewBag.len = this.storage.GetRepository<EventsRepository>().len();
                return this.View(this.storage.GetRepository<EventsRepository>().Nyne().ToList());

        }

        [Authorize]
        [Authorize(Policy = "User")]
        public ActionResult StatusUser()
        {
            string UserId = ThisUser(User.Identity.Name).Id;
            ViewBag.Chat = this.storage.GetRepository<ChatRepository>().ChatByUserId(UserId);
            return this.View(this.storage.GetRepository<UserRepository>().UserById(UserId));
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult StatusAdmin()
        {
            string UserId = ThisUser(User.Identity.Name).Id;
            ViewBag.Chat = this.storage.GetRepository<ChatRepository>().ChatByUserId(UserId);
            return this.View(this.storage.GetRepository<UserRepository>().UserById(UserId));
        }

        public ActionResult Tic(int id)
        {
            
            return this.View(this.storage.GetRepository<TicketRepository>().TicketByIdEventStatus(id).ToList());
        }

        [Authorize]
        [HttpGet]
        public ActionResult NewTicket(int a, string city, string data, string name)
        {
            ViewBag.a = a;
            List<Events> l = this.storage.GetRepository<EventsRepository>().All().ToList();
           ViewBag.Event = l;
            return this.View(new TicketViewModel { });
        }

        [Authorize]
        [HttpPost]
        public ActionResult NewTicket(TicketViewModel file)
        {
            if(file.Name==null)
            {
                return RedirectToAction("NewTicket", "Home", new { a = 3 });
            }
            if(file.Price==null)
            {
                return RedirectToAction("NewTicket", "Home", new { a = 2 });
            }
            int x = 0;
            bool result = Int32.TryParse(file.Price, out x);
            if (result == false)
            {
                return RedirectToAction("NewTicket", "Home", new { a = 2 });
            }
            Venue v = this.storage.GetRepository<VenueRepository>().VenueFind(file.Venue,file.VenueAddress,file.VenueCity);
            Events e = this.storage.GetRepository<EventsRepository>().EventsByInfo(file.Name,file.Date,v);
            Ticket t = new Ticket(e,file.Price, ThisUser(User.Identity.Name));
            this.storage.GetRepository<TicketRepository>().TicketAdd(t);
            return RedirectToAction("NewTicket", "Home", new { a = 1 });
        }


        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpGet]
        public ActionResult NewCity(int a)
        {
            ViewBag.City = this.storage.GetRepository<CityRepository>().CityAll();
            ViewBag.Event = this.storage.GetRepository<EventsRepository>().All();
            ViewBag.Venue = this.storage.GetRepository<VenueRepository>().VenueAll();
            ViewBag.miss = a;
            return this.View(new EventViewModel { });
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewCityChange(String name, String newName)
        {
           if(name==null || newName==null)
           {
               return RedirectToAction("NewCity", "Home", new { a = 1 });
           }
            if (!this.storage.GetRepository<CityRepository>().UniquenessCity(name))
            {
                this.storage.GetRepository<CityRepository>().ChangeCity(name, newName);
                return RedirectToAction("NewCity", "Home", new { a = 3 });
            }
            else
            {
                return RedirectToAction("NewCity", "Home", new { a = 4 });
            }
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewCityAdd(String name)
        {
            if (name == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 1 });
            }
            if(!this.storage.GetRepository<CityRepository>().UniquenessCity(name))
            {
                return RedirectToAction("NewCity", "Home", new { a = 2 });
            }
            else
            {
                this.storage.GetRepository<CityRepository>().CityAdd(name);
                return RedirectToAction("NewCity", "Home", new { a = 3 });
            }                          
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewCityDel(String name)
        {
            if (name == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 1 });
            }
            if (this.storage.GetRepository<CityRepository>().UniquenessCity(name))
            {
                return RedirectToAction("NewCity", "Home", new { a = 4 });
            }
            else
            {
                if (this.storage.GetRepository<CityRepository>().DelCity(name))
                {
                    return RedirectToAction("NewCity", "Home", new { a = 3 });
                }
                else
                {
                    return RedirectToAction("NewCity", "Home", new { a = 5 });
                }
            }
   
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewVenueChange(String name, String address,String cityName, String newName, String newAddress, String newCityName)
        {
            if (name == null || address==null || cityName==null || newName==null || newAddress ==null || newCityName == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 7 });
            }
            if(this.storage.GetRepository<VenueRepository>().UniquenesVenue(name, address,cityName))
            {
               
                return RedirectToAction("NewCity", "Home", new { a = 9 });
            }
            else
            {
                if (this.storage.GetRepository<CityRepository>().UniquenessCity(newCityName))
                {
                    return RedirectToAction("NewCity", "Home", new { a = 10 });
                }
                else
                {
                     this.storage.GetRepository<VenueRepository>().VenueUpdate(this.storage.GetRepository<VenueRepository>().VenueFind(name, address, cityName),newName,newAddress,newCityName);
                    return RedirectToAction("NewCity", "Home", new { a = 6 });
                }
            }
            
            
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewVenueAdd(String newName, String newAddress, String newCityName)
        {
            if (newName == null || newAddress == null || newCityName == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 7 });
            }
            if (!this.storage.GetRepository<VenueRepository>().UniquenesVenue(newName, newAddress, newCityName))
            {

                return RedirectToAction("NewCity", "Home", new { a = 8 });
            }
            else
            {
                if (this.storage.GetRepository<CityRepository>().UniquenessCity(newCityName))
                {
                    return RedirectToAction("NewCity", "Home", new { a = 10 });
                }
                else
                {
                    this.storage.GetRepository<VenueRepository>().VenueAdd(new Venue(newName, newAddress, this.storage.GetRepository<CityRepository>().CityByName(newCityName)));
                    return RedirectToAction("NewCity", "Home", new { a = 6 });
                }
            }
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult NewVenueDel(String name, String address, String cityName)
        {
            if (name == null || address == null || cityName == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 7 });
            }
            if (this.storage.GetRepository<VenueRepository>().UniquenesVenue(name, address, cityName))
            {

                return RedirectToAction("NewCity", "Home", new { a = 9 });
            }
            else
            {
                if (this.storage.GetRepository<VenueRepository>().VenueDel(this.storage.GetRepository<VenueRepository>().VenueFind(name, address, cityName)))
                {
                    return RedirectToAction("NewCity", "Home", new { a = 6 });
                }
                else
                {
                    return RedirectToAction("NewCity", "Home", new { a = 5 });
                }
            }
        }


        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<IActionResult> NewCity(EventViewModel file)
        {
            DateTime myDate;
            if (!DateTime.TryParse(file.Date, out myDate))
            {
                return RedirectToAction("NewCity", "Home", new { a = 18 });
            }
            if (DateTime.Now > myDate)
            {
                return RedirectToAction("NewCity", "Home", new { a = 18 });
            }

            if (file.Name==null || file.Date==null || file.Venue==null || file.OldName==null || file.OldDate == null || file.OldVenue == null || file.OldVenueCity == null || file.OldVenueAddress  == null || file.VenueCity == null || file.VenueAddress == null )
            {
                return RedirectToAction("NewCity", "Home", new { a = 11});
            }

            if (this.storage.GetRepository<VenueRepository>().VenueFind(file.Venue, file.VenueAddress, file.VenueCity) == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 12 });
            }

            Venue oldVenue = this.storage.GetRepository<VenueRepository>().VenueFind(file.OldVenue, file.OldVenueAddress, file.OldVenueCity);
           if (oldVenue == null)
           {
               return RedirectToAction("NewCity", "Home", new { a = 12 });
           }

            Events olde, newe;
            olde = this.storage.GetRepository<EventsRepository>().EventsByInfo(file.OldName, file.OldDate, oldVenue);

            if (olde == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 14 });
            }

            newe = new Events(file.Name, file.Date, this.storage.GetRepository<VenueRepository>().VenueFind(file.Venue, file.VenueAddress, file.VenueCity), olde.Image,file.Description);
            if (file.Photo!=null)
            {
                try
                {
                    string fileName = string.Concat(DateTime.Now.ToString("yyyy-MM-dd HHmmtt"), file.Photo.FileName);
                    using (var fileStream = new FileStream(Path.Combine(environment.WebRootPath, "images", fileName), FileMode.CreateNew))
                    {
                        await file.Photo.CopyToAsync(fileStream);
                    }
                    newe.Image = fileName;
                }
                catch
                {
                    return RedirectToAction("NewCity", "Home", new { a = 15 });
                }
            }

            this.storage.GetRepository<EventsRepository>().EventsUpdate(olde, newe);
            return RedirectToAction("NewCity", "Home", new { a = 13 });
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        public ActionResult EventDel(String name, String data, String venue, String city,String address)
        {
            if (name==null || data==null|| venue == null || city == null || address== null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 11 });
            }

            Venue v = this.storage.GetRepository<VenueRepository>().VenueFind(venue, address, city);
            if (v==null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 12 });
            }

            Events events = this.storage.GetRepository<EventsRepository>().EventsByInfo(name, data, v);
            if (events == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 14 });
            }

           if( !this.storage.GetRepository<EventsRepository>().EventsDel(events))
            {
                return RedirectToAction("NewCity", "Home", new { a = 16 });
            }
            else
            {
                return RedirectToAction("NewCity", "Home", new { a = 13 });
            }
            
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpGet]
        public ActionResult EventAdd()
        {
            return PartialView(new EventViewModel { });
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<ActionResult> EventAdd(EventViewModel file)
        {
            DateTime myDate;
            if (!DateTime.TryParse(file.Date, out myDate))
            {
                    return RedirectToAction("NewCity", "Home", new { a = 18 });
            }

            if (DateTime.Now > myDate)
            {
                return RedirectToAction("NewCity", "Home", new { a = 18 });
            }

            if (file.Name == null || file.Date == null || file.Venue == null  || file.VenueCity == null || file.VenueAddress == null)
            {
                return RedirectToAction("NewCity", "Home", new { a = 11 });
            }


            if (file.Photo != null)
            {
                try
                {
                    Random rnd = new Random();
                    string value ="rand"+ rnd.Next(0, 1000).ToString();
                    string fileName = string.Concat(string.Concat(DateTime.Now.ToString("yyyy-MM-dd HHmmtt"), value),  file.Photo.FileName);
                     using (var fileStream = new FileStream(Path.Combine(environment.WebRootPath, "images", fileName), FileMode.CreateNew))
                     {
                         await file.Photo.CopyToAsync(fileStream);
                     }
                    Venue venue = this.storage.GetRepository<VenueRepository>().VenueFind(file.Venue, file.VenueAddress, file.VenueCity);
                    if (venue == null)
                    {
                        return RedirectToAction("NewCity", "Home", new { a = 12 });
                    }
                   Events events = this.storage.GetRepository<EventsRepository>().EventsByInfo(file.Name,file.Date, venue);
                    if (events == null)
                    {
                        this.storage.GetRepository<EventsRepository>().EventsAdd(new Events(file.Name,file.Date,venue,fileName,file.Description));
                        return RedirectToAction("NewCity", "Home", new { a = 13 });
                    }
                    else
                    {
                        return RedirectToAction("NewCity", "Home", new { a = 17 });
                    }
                }
                catch
                {
                    return RedirectToAction("NewCity", "Home", new { a = 15 });
                }
            }
            else
            {
                return RedirectToAction("NewCity", "Home", new { a = 11 });
            }   
        }

        [Authorize]
        public ActionResult Bay(int idTicket)
        {
            ViewBag.MissTex = "false";
            Ticket t = this.storage.GetRepository<TicketRepository>().TicketById(idTicket);
            User us = this.storage.GetRepository<UserRepository>().UserByName(User.Identity.Name).ToList()[0];
            if (!this.storage.GetRepository<OrderRepository>().UniqueOrder(us, t))
            {
                ViewBag.MissTex = "true";
            }
            else
            {
                this.storage.GetRepository<OrderRepository>().NewOrder(new Order(t, "В ожидании", us, "trackNO"));
                string s = "Пользователь " + us.FirstName+ " хочет купить ваш билет на <<" +t.Events.Name + ">> проверьте ваши билеты!";
                this.storage.GetRepository<ChatRepository>().ChatAdd(new Chat(s,t.Seller,us,DateTime.Now));
            }            
            return this.View();
        }


        [HttpGet]
        public IActionResult Reg(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }
        [HttpPost]
        public async Task<IActionResult> Reg(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User curUser = await customUserManager.FindByNameAsync(model.Username);
                if (curUser != null)
                {
                    if (await customUserManager.CheckPasswordAsync(curUser, model.Password))
                    {
                        await signInManager.SignInAsync(curUser, model.RememberMe);
                        return RedirectToAction("IndexLog", "Home"); //если все секс
                    }
                    else
                    {
                        ModelState.AddModelError("LogErr", "Incorrect Password");
                    }

                }
                else
                {
                    ModelState.AddModelError("LogErr", "Incorrect Login");
                }
            }
            ViewBag.Miss = ModelState.ErrorCount;
            return View(model);
        }

        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpGet]
        public IActionResult RoleSettings()
        {
            return this.View(new UserViewModel { });
        }
        [Authorize]
        [Authorize(Policy = "Admin")]
        [HttpPost]
        public IActionResult RoleSettings(UserViewModel userNewInfo)
        {
            User us;
            try
            {
                 us = this.storage.GetRepository<UserRepository>().UserByName(userNewInfo.UserName).ToList()[0];
            }
            catch(System.ArgumentNullException)
            {
                us = null;
                ViewBag.MissTipe = "1";
                return View(userNewInfo);
            }
            catch (System.ArgumentOutOfRangeException)
            {
                us = null;
                ViewBag.MissTipe = "2";
                return View(userNewInfo);
            }
            if (ThisUser(User.Identity.Name).UserName== userNewInfo.UserName)
            {
                us = null;
                ViewBag.MissTipe = "3";
                return View(userNewInfo);
            }
            ViewBag.Table = "true";
            ViewBag.User = us;
            ViewBag.UserRole = this.storage.GetRepository<RoleStatusOfUserRepository>().RoleByUser(us).Role.Name;
            return View(userNewInfo);
        }
        public IActionResult RoleSettingsChange(String name)
        {
           this.storage.GetRepository<RoleStatusOfUserRepository>().UpdataRole(this.storage.GetRepository<RoleStatusOfUserRepository>().RoleByUser(this.storage.GetRepository<UserRepository>().UserByName(name).ToList()[0]));
            return RedirectToAction("RoleSettings", "Home");
        }

        [Authorize]
        [HttpGet]
        public IActionResult Settings()
        {
            User us = this.storage.GetRepository<UserRepository>().UserById(ThisUser(User.Identity.Name).Id).ToList()[0];
            return this.View(new UserViewModel {Address=us.Address,PhoneNumber=us.PhoneNumber, Localization =us.Localization});
        }
        [Authorize]
        [HttpPost]
        public IActionResult Settings(UserViewModel userNewInfo)
        {
            if (userNewInfo.Address==null || userNewInfo.Localization==null || userNewInfo.PhoneNumber==null)
            {
                ViewBag.missTipe = "1";
                return View(userNewInfo);
            }
            if (userNewInfo.PhoneNumber.Length !=7)
            {
                ViewBag.missTipe = "2";
                return View(userNewInfo);
            }
            try
            {
                Convert.ToInt32(userNewInfo.PhoneNumber);
                User us = this.storage.GetRepository<UserRepository>().UserById(ThisUser(User.Identity.Name).Id).ToList()[0];
                us.Address = userNewInfo.Address;
                us.Localization = userNewInfo.Localization;
                us.PhoneNumber = userNewInfo.PhoneNumber;
                this.storage.GetRepository<UserRepository>().UserUpdate(us);
                return RedirectToAction("IndexLog", "Home");
            }
            catch
            {
                ViewBag.missTipe = "2";
                return View(userNewInfo);
            }
        }


        [HttpGet]
        public IActionResult Reg2(string returnUrl = null)
        {
            ViewBag.MissLog = "false";
            ViewBag.MissVoid = "false";
            ViewBag.MissPhone = "false";
            ViewBag.MissPhoneInt = "false";
            return View(new RegViewModel { ReturnUrl = returnUrl });
        }
        [HttpPost]
        public async Task<IActionResult> Reg2(RegViewModel model)
        {
            ViewBag.MissLog = "false";
            ViewBag.MissVoid = "false";
            ViewBag.MissPhone = "false";
            ViewBag.MissPhoneInt = "false";
            ViewBag.MissReg = "false";
            if (model.Address == null || model.LastName == null || model.Localization==null || model.Name==null || model.Password==null || model.Phone==null || model.Username==null)
            {
                ViewBag.MissVoid = "true";
                return View(model);
            }
            else
            {
                if (model.Phone.Length != 7)
                {
                    ViewBag.MissPhone = "true";
                    return View(model);
                }
                else
                {
                    try
                    {
                        Convert.ToInt32(model.Phone);
                        User user = new User(model.Name, model.LastName, model.Localization, model.Address, model.Phone, model.Username, model.Password);
                        if (this.storage.GetRepository<UserRepository>().UniquenessUser(user))
                        {
                            this.storage.GetRepository<UserRepository>().AddUserThisRole(user);
                            ViewBag.MissReg = "true";
                            return View(model);
                        }
                        else
                        {
                            ViewBag.MissLog = "true";
                            return View(model);
                        }
                    }
                    catch (FormatException)
                    {
                        ViewBag.MissPhoneInt = "true";
                        return View(model);
                    }
                }
            }
        }

        [Authorize]
        public void Out()
        {
            signInManager.SignOutAsync();
            Response.Redirect("/Home/Index");
        }

       [Authorize]
        public ActionResult MyTickets()
        {
            ViewBag.UsT = this.storage.GetRepository<TicketRepository>().TicketExplosed(ThisUser(User.Identity.Name).Id);
            return this.View(this.storage.GetRepository<OrderRepository>().AllForIdUser(ThisUser(User.Identity.Name).Id).ToList());
        }

        [Authorize]
        public ActionResult MyTicketsAccept(int id,string message)
        {
            message = " принят! Коментарий от продовца: " + message;
            this.storage.GetRepository<ChatRepository>().ChatAddForBuy(id,message, ThisUser(User.Identity.Name));
            this.storage.GetRepository<OrderRepository>().OrderStatusSell(id);
            this.storage.GetRepository<ChatRepository>().ChatAvtoDel(id,ThisUser(User.Identity.Name));
            this.storage.GetRepository<OrderRepository>().OrderDellStatusNoSell(id);
            this.storage.GetRepository<TicketRepository>().StatusNoSell(this.storage.GetRepository<OrderRepository>().OrderById(id).Ticket);
            return RedirectToAction("MyTickets", "Home");
        }

        [Authorize]
        public ActionResult MyTicketsDel(int id)
        {
            this.storage.GetRepository<TicketRepository>().TicketDelFull(id, ThisUser(User.Identity.Name));
            return RedirectToAction("MyTickets", "Home");
        }


        [Authorize]
        public ActionResult MyTicketsNoAccept(int id, string message)
        {
            message = " отклонен! Коментарий от продовца: " + message;
            this.storage.GetRepository<ChatRepository>().ChatAddForBuy(id, message, ThisUser(User.Identity.Name));
            this.storage.GetRepository<OrderRepository>().OrderDel(id);
            return RedirectToAction("MyTickets", "Home");
        }

        [Authorize]
        public ActionResult MyOrder()
        {
            return this.View(this.storage.GetRepository<OrderRepository>().AllForIdUserBay(ThisUser(User.Identity.Name).Id).ToList());
        }

        public User ThisUser(string name)
        {
            return this.storage.GetRepository<UserRepository>().UserByName(name).ToList()[0];
        }
    }
}

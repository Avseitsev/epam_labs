﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using BelTicket.Models;
using BelTicket.Models.Register;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BelTicket.Controllers
{
    [Route("api/[controller]")]
    public class CityController : Controller
    {
        private CustomUserManager customUserManager;
        private SignInManager<User> signInManager;
        private IStorage storage;
        private IHostingEnvironment environment;

        public CityController(IHostingEnvironment environment, IStorage storage, CustomUserManager customUserManage, SignInManager<User> signInManager)
        {
            this.signInManager = signInManager;
            this.customUserManager = customUserManage;
            this.storage = storage;
            this.environment = environment;
        }


      //   GET: api/values
      [HttpGet]
      public IEnumerable<City> Get(string name)
      {
          return storage.GetRepository<CityRepository>().AllForApi(name).ToList();
      }

    }      
}

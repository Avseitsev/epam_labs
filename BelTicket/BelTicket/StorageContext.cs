﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelTicket.inter;
using BelTicket.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BelTicket
{
    public class StorageContext : IdentityDbContext<User>,IStorageContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<City> Citys { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleStatusOfUser> RoleStatusOfUser { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Chat> Chat { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>().ToTable("Cities");
            modelBuilder.Entity<Venue>().ToTable("Venues");
            modelBuilder.Entity<Ticket>().ToTable("Tickets");
            modelBuilder.Entity<RoleStatusOfUser>().ToTable("RoleStatusOfUser");
            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<Events>().ToTable("Events");
            modelBuilder.Entity<Chat>().ToTable("Chat");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ResaleDB_Pavel_Avseitsev;Trusted_Connection=True;");
        }
    }
}


﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Tests.ViewModels
{
    public sealed class TaskViewModel : PropertyChangedBase, INotifyPropertyChanged
    {
        private int zaprosi;
        private int duration;
        private int potoci;
        private string url;

        public int Zaprosi
        {
            get { return zaprosi; }
            set
            {
                zaprosi = value;
                OnPropertyChanged("Zaprosi");
            }
        }
        public int Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                OnPropertyChanged("Duration");
            }
        }
        public int Potoci
        {
            get { return potoci; }
            set
            {
                potoci = value;
                OnPropertyChanged("Potoci");
            }
        }
        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                OnPropertyChanged("Title");
            }
        }


        public TaskViewModel()
        {
        }

        public TaskViewModel(int zaprosi, int duration, int potoci, string url)
        {
            this.zaprosi = zaprosi;
            this.duration = duration;
            this.potoci = potoci;
            this.url = url;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
